#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: user
#------------------------------------------------------------

CREATE TABLE user(
        id_user                     Int  Auto_increment  NOT NULL ,
        name_user                   Varchar (50) NOT NULL ,
        f_name_user                 Varchar (50) NOT NULL ,
        date_birth_user             Date NOT NULL ,
        account_tokens_user         Int NOT NULL ,
        account_tokens_blocked_user Int NOT NULL ,
        admin_bool_user             Bool NOT NULL ,
        tel_user                    Varchar (20) NOT NULL
	,CONSTRAINT user_PK PRIMARY KEY (id_user)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: category
#------------------------------------------------------------

CREATE TABLE category(
        id_category   Int  Auto_increment  NOT NULL ,
        name_category Varchar (50) NOT NULL
	,CONSTRAINT category_PK PRIMARY KEY (id_category)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: product
#------------------------------------------------------------

CREATE TABLE product(
        id_product           Int  Auto_increment  NOT NULL ,
        name_product         Varchar (50) NOT NULL ,
        manufacturer_product Varchar (50) NOT NULL
	,CONSTRAINT product_PK PRIMARY KEY (id_product)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: announce
#------------------------------------------------------------

CREATE TABLE announce(
        id_announce              Int  Auto_increment  NOT NULL ,
        date_online_announce     Datetime NOT NULL ,
        date_validated_announce  Datetime NOT NULL ,
        picture_product_announce Varchar (500) NOT NULL ,
        status_product_announce  Varchar (200) NOT NULL ,
        finish_product_announce  Varchar (200) NOT NULL ,
        money_announce           Float NOT NULL ,
        id_user                  Int NOT NULL ,
        id_product               Int NOT NULL
	,CONSTRAINT announce_PK PRIMARY KEY (id_announce)

	,CONSTRAINT announce_user_FK FOREIGN KEY (id_user) REFERENCES user(id_user)
	,CONSTRAINT announce_product0_FK FOREIGN KEY (id_product) REFERENCES product(id_product)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: keywords
#------------------------------------------------------------

CREATE TABLE keywords(
        id_keyword Int  Auto_increment  NOT NULL ,
        keyword    Varchar (50) NOT NULL
	,CONSTRAINT keywords_PK PRIMARY KEY (id_keyword)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: product_keyword
#------------------------------------------------------------

CREATE TABLE product_keyword(
        id_product Int NOT NULL ,
        id_keyword Int NOT NULL
	,CONSTRAINT product_keyword_PK PRIMARY KEY (id_product,id_keyword)

	,CONSTRAINT product_keyword_product_FK FOREIGN KEY (id_product) REFERENCES product(id_product)
	,CONSTRAINT product_keyword_keywords0_FK FOREIGN KEY (id_keyword) REFERENCES keywords(id_keyword)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: category_product
#------------------------------------------------------------

CREATE TABLE category_product(
        id_product  Int NOT NULL ,
        id_category Int NOT NULL
	,CONSTRAINT category_product_PK PRIMARY KEY (id_product,id_category)

	,CONSTRAINT category_product_product_FK FOREIGN KEY (id_product) REFERENCES product(id_product)
	,CONSTRAINT category_product_category0_FK FOREIGN KEY (id_category) REFERENCES category(id_category)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: user_announce_wtb
#------------------------------------------------------------

CREATE TABLE user_announce_wtb(
        id_announce                   Int NOT NULL ,
        id_user                       Int NOT NULL ,
        date_user_announce_wtb        Datetime NOT NULL ,
        description_user_announce_wtb Varchar (1000) NOT NULL ,
        money_user_announce_wtb       Float NOT NULL ,
        status_user_announce_wtb      Bool NOT NULL
	,CONSTRAINT user_announce_wtb_PK PRIMARY KEY (id_announce,id_user)

	,CONSTRAINT user_announce_wtb_announce_FK FOREIGN KEY (id_announce) REFERENCES announce(id_announce)
	,CONSTRAINT user_announce_wtb_user0_FK FOREIGN KEY (id_user) REFERENCES user(id_user)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: user_announce_comment
#------------------------------------------------------------

CREATE TABLE user_announce_comment(
        id_announce                       Int NOT NULL ,
        id_user                           Int NOT NULL ,
        description_user_announce_comment Varchar (1000) NOT NULL
	,CONSTRAINT user_announce_comment_PK PRIMARY KEY (id_announce,id_user)

	,CONSTRAINT user_announce_comment_announce_FK FOREIGN KEY (id_announce) REFERENCES announce(id_announce)
	,CONSTRAINT user_announce_comment_user0_FK FOREIGN KEY (id_user) REFERENCES user(id_user)
)ENGINE=InnoDB;

