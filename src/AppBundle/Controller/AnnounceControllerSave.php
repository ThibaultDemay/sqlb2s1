<?php

namespace AppBundle\Controller;

use AppBundle\Manager\AnnounceManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AnnounceControllerSave extends Controller
{
    private function getManager()
    {
        return new AnnounceManager($this->getDoctrine()->getManager());
    }


    /** * @Route("/announce/index", name="announce_index") */
    public function indexAction()
    {// Obtention du manager puis des annonces
        $announces = $this->getManager()->loadAllAnnounces();
        return $this->render('announces/index.html.twig', array("arrayAnnounces" => $announces));
    }


}

