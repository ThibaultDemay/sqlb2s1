<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class homepageController extends Controller
{
    /**
     * @Route ("admin/homepage", name ="admin/homepage_index")
     */
    public function indexAction()
    {
        $message = "Bonjour Visiteur sur notre site de revente d'instruments de musique !";
        return $this->render('admin/homepage/index.html.twig', array('message' => $message));
    }
}
