<?php

namespace AppBundle\Controller;

use AppBundle\Manager\CategoryManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CategoryControllerSave extends Controller
{
    private function getManager()
    {
        return new CategoryManager($this->getDoctrine()->getManager());
    }

    /** * @Route("/category/index", name="category_index") */
    public function indexAction()
    {
        // Obtention du manager puis des categorys
        $categorys = $this->getManager()->loadAllCategorys();
        return $this->render('category/index.html.twig', array("arrayCategorys" => $categorys));
    }
}
