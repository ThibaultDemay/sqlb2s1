<?php

namespace AppBundle\Controller;

use AppBundle\Manager\KeywordsManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class KeywordsControllerSave extends Controller
{
    private function getManager()
    {
        return new KeywordsManager($this->getDoctrine()->getManager());
    }

    /** * @Route("/keywords/index", name="keywords_index") */
    public function indexAction()
    {
        // Obtention du manager puis des keywords
        $keywordss = $this->getManager()->loadAllKeywordss();
        return $this->render('keywords/index.html.twig', array("arrayKeywordss" => $keywordss));
    }
}
