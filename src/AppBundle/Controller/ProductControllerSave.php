<?php

namespace AppBundle\Controller;

use AppBundle\Manager\ProductManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProductControllerSave extends Controller
{
    private function getManager()
    {
        return new ProductManager($this->getDoctrine()->getManager());
    }

    /** * @Route("/product/index", name="product_index") */
    public function indexAction()
    {
        // Obtention du manager puis des products
        $products = $this->getManager()->loadAllProducts();
        return $this->render('product/index.html.twig', array("arrayProducts" => $products));
    }
}
