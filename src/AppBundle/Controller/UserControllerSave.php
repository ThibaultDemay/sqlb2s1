<?php

namespace AppBundle\Controller;

use AppBundle\Manager\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserControllerSave extends Controller
{
    private function getManager()
    {
        return new UserManager($this->getDoctrine()->getManager());
    }

    /** * @Route("/user/index", name="user_index") */
    public function indexAction()
    {
        // Obtention du manager puis des users
        $users = $this->getManager()->loadAllUsers();
        return $this->render('user/index.html.twig', array("arrayUsers" => $users));
    }
}
