<?php
/**
 * Created by PhpStorm.
 * User: bouch
 * Date: 05/10/2018
 * Time: 15:07
 */


namespace AppBundle\Manager;

use AppBundle\Entity\Category;
use AppBundle\Entity\CategoryRepository;
use Doctrine\ORM\EntityManager;

class CategoryManager
{
    protected $entityManager;
    protected $repository;

    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
        $this->repository = $em->getRepository('AppBundle:Category');
    }

    public function loadAllCategorys()
    {
        $categorys = $this->repository->findAll();
        return $categorys;
    }

    /** * Load Category entity * * @param Integer $categoryId */
    public function loadCategory($categoryId)
    {
        return $this->repository->find($categoryId);
    }

    /** * Save Category entity * * @param Category $category */
    public function saveCategory(Category $category)
    {
        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }

    /**
     * Remove Category entity * * @param Integer $categoryId
     */
    public function removeCategory(Category $category)
    {
        $this->entityManager->remove($category);
        $this->entityManager->flush();
    }
}