<?php
/**
 * Created by PhpStorm.
 * User: bouch
 * Date: 05/10/2018
 * Time: 15:14
 */

namespace AppBundle\Manager;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductRepository;
use Doctrine\ORM\EntityManager;

class ProductManager
{
    protected $entityManager;
    protected $repository;

    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
        $this->repository = $em->getRepository('AppBundle:Product');
    }

    public function loadAllProducts()
    {
        $products = $this->repository->findAll();
        return $products;
    }

    /** * Load Product entity * * @param Integer $productId */
    public function loadProduct($productId)
    {
        return $this->repository->find($productId);
    }

    /** * Save Product entity * * @param Product $product */
    public function saveProduct(Product $product)
    {
        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }

    /**
     * Remove Product entity * * @param Integer $productId
     */
    public function removeProduct(Product $product)
    {
        $this->entityManager->remove($product);
        $this->entityManager->flush();
    }
}