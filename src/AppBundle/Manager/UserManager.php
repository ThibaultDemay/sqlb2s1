<?php
/**
 * Created by PhpStorm.
 * User: bouch
 * Date: 05/10/2018
 * Time: 15:15
 */

namespace AppBundle\Manager;

use AppBundle\Entity\User;
use AppBundle\Entity\UserRepository;
use Doctrine\ORM\EntityManager;

class UserManager
{
    protected $entityManager;
    protected $repository;

    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
        $this->repository = $em->getRepository('UserSave');
    }

    public function loadAllUsers()
    {
        $users = $this->repository->findAll();
        return $users;
    }

    /** * Load User entity * * @param Integer $userId */
    public function loadUser($userId)
    {
        return $this->repository->find($userId);
    }

    /** * Save User entity * * @param User $user */
    public function saveUser(User $user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * Remove User entity * * @param Integer $userId
     */
    public function removeUser(User $user)
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }
}