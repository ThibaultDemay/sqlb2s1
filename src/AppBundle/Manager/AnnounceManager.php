<?php
/**
 * Created by PhpStorm.
 * User: Thibault
 * Date: 05/10/2018
 * Time: 15:07
 */

namespace AppBundle\Manager;

use AppBundle\Entity\Announce;
use AppBundle\Entity\AnnounceRepository;
use Doctrine\ORM\EntityManager;

class AnnounceManager
{
    protected $entityManager;
    protected $repository;

    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
        $this->repository = $em->getRepository('AppBundle:Announce');
    }

    public function loadAllAnnounces()
    {
        $announces = $this->repository->findAll();
        return $announces;
    }

    /**
     * Load Announces entity
     *
     * @param Integer $announceId
     */
    public function loadAnnounce($AnnounceId)
    {
        return $this->repository->find($AnnounceId);
    }

    public function saveAnnounce(Announce $announce)
    {
        $this->entityManager->persist($announce);
        $this->entityManager->flush();
    }

    /**
     * Remove Announce entity
     *
     * @param Integer $announceId
     */
    public function removeAnnonce(Announce $annonce)
    {
        $this->entityManager->remove($announce);
        $this->entityManager->flush();
    }
}