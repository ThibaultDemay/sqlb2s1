<?php
/**
 * Created by PhpStorm.
 * User: bouch
 * Date: 05/10/2018
 * Time: 15:16
 */

namespace AppBundle\Manager;

use AppBundle\Entity\Keywords;
use AppBundle\Entity\KeywordsRepository;
use Doctrine\ORM\EntityManager;

class KeywordsManager
{
    protected $entityManager;
    protected $repository;

    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
        $this->repository = $em->getRepository('AppBundle:Keywords');
    }

    public function loadAllKeywordss()
    {
        $keywordss = $this->repository->findAll();
        return $keywordss;
    }

    /** * Load Keywords entity * * @param Integer $keywordsId */
    public function loadKeywords($keywordsId)
    {
        return $this->repository->find($keywordsId);
    }

    /** * Save Keywords entity * * @param Keywords $keywords */
    public function saveKeywords(Keywords $keywords)
    {
        $this->entityManager->persist($keywords);
        $this->entityManager->flush();
    }

    /**
     * Remove Keywords entity * * @param Integer $keywordsId
     */
    public function removeKeywords(Keywords $keywords)
    {
        $this->entityManager->remove($keywords);
        $this->entityManager->flush();
    }
}