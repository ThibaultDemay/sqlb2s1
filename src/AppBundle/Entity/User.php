<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity (repositoryClass="AppBundle\Entity\UserRepository")
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_user", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="name_user", type="string", length=50, nullable=false)
     */
    private $nameUser;

    /**
     * @var string
     *
     * @ORM\Column(name="f_name_user", type="string", length=50, nullable=false)
     */
    private $fNameUser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_birth_user", type="date", nullable=false)
     */
    private $dateBirthUser;

    /**
     * @var integer
     *
     * @ORM\Column(name="account_tokens_user", type="integer", nullable=false)
     */
    private $accountTokensUser;

    /**
     * @var integer
     *
     * @ORM\Column(name="account_tokens_blocked_user", type="integer", nullable=false)
     */
    private $accountTokensBlockedUser;

    /**
     * @var boolean
     *
     * @ORM\Column(name="admin_bool_user", type="boolean", nullable=false)
     */
    private $adminBoolUser;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_user", type="string", length=20, nullable=false)
     */
    private $telUser;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Announce", mappedBy="idUserUserAnnounceComment")
     */
    private $idAnnounceUserAnnounceComment;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Announce", mappedBy="idUserUserAnnounceWtb")
     */
    private $idAnnounceUserAnnounceWtb;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idAnnounceUserAnnounceComment = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idAnnounceUserAnnounceWtb = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idUser
     *
     * @return integer
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set nameUser
     *
     * @param string $nameUser
     *
     * @return User
     */
    public function setNameUser($nameUser)
    {
        $this->nameUser = $nameUser;

        return $this;
    }

    /**
     * Get nameUser
     *
     * @return string
     */
    public function getNameUser()
    {
        return $this->nameUser;
    }

    /**
     * Set fNameUser
     *
     * @param string $fNameUser
     *
     * @return User
     */
    public function setFNameUser($fNameUser)
    {
        $this->fNameUser = $fNameUser;

        return $this;
    }

    /**
     * Get fNameUser
     *
     * @return string
     */
    public function getFNameUser()
    {
        return $this->fNameUser;
    }

    /**
     * Set dateBirthUser
     *
     * @param \DateTime $dateBirthUser
     *
     * @return User
     */
    public function setDateBirthUser($dateBirthUser)
    {
        $this->dateBirthUser = $dateBirthUser;

        return $this;
    }

    /**
     * Get dateBirthUser
     *
     * @return \DateTime
     */
    public function getDateBirthUser()
    {
        return $this->dateBirthUser;
    }

    /**
     * Set accountTokensUser
     *
     * @param integer $accountTokensUser
     *
     * @return User
     */
    public function setAccountTokensUser($accountTokensUser)
    {
        $this->accountTokensUser = $accountTokensUser;

        return $this;
    }

    /**
     * Get accountTokensUser
     *
     * @return integer
     */
    public function getAccountTokensUser()
    {
        return $this->accountTokensUser;
    }

    /**
     * Set accountTokensBlockedUser
     *
     * @param integer $accountTokensBlockedUser
     *
     * @return User
     */
    public function setAccountTokensBlockedUser($accountTokensBlockedUser)
    {
        $this->accountTokensBlockedUser = $accountTokensBlockedUser;

        return $this;
    }

    /**
     * Get accountTokensBlockedUser
     *
     * @return integer
     */
    public function getAccountTokensBlockedUser()
    {
        return $this->accountTokensBlockedUser;
    }

    /**
     * Set adminBoolUser
     *
     * @param boolean $adminBoolUser
     *
     * @return User
     */
    public function setAdminBoolUser($adminBoolUser)
    {
        $this->adminBoolUser = $adminBoolUser;

        return $this;
    }

    /**
     * Get adminBoolUser
     *
     * @return boolean
     */
    public function getAdminBoolUser()
    {
        return $this->adminBoolUser;
    }

    /**
     * Set telUser
     *
     * @param string $telUser
     *
     * @return User
     */
    public function setTelUser($telUser)
    {
        $this->telUser = $telUser;

        return $this;
    }

    /**
     * Get telUser
     *
     * @return string
     */
    public function getTelUser()
    {
        return $this->telUser;
    }

    /**
     * Add idAnnounceUserAnnounceComment
     *
     * @param \AppBundle\Entity\Announce $idAnnounceUserAnnounceComment
     *
     * @return User
     */
    public function addIdAnnounceUserAnnounceComment(\AppBundle\Entity\Announce $idAnnounceUserAnnounceComment)
    {
        $this->idAnnounceUserAnnounceComment[] = $idAnnounceUserAnnounceComment;

        return $this;
    }

    /**
     * Remove idAnnounceUserAnnounceComment
     *
     * @param \AppBundle\Entity\Announce $idAnnounceUserAnnounceComment
     */
    public function removeIdAnnounceUserAnnounceComment(\AppBundle\Entity\Announce $idAnnounceUserAnnounceComment)
    {
        $this->idAnnounceUserAnnounceComment->removeElement($idAnnounceUserAnnounceComment);
    }

    /**
     * Get idAnnounceUserAnnounceComment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdAnnounceUserAnnounceComment()
    {
        return $this->idAnnounceUserAnnounceComment;
    }

    /**
     * Add idAnnounceUserAnnounceWtb
     *
     * @param \AppBundle\Entity\Announce $idAnnounceUserAnnounceWtb
     *
     * @return User
     */
    public function addIdAnnounceUserAnnounceWtb(\AppBundle\Entity\Announce $idAnnounceUserAnnounceWtb)
    {
        $this->idAnnounceUserAnnounceWtb[] = $idAnnounceUserAnnounceWtb;

        return $this;
    }

    /**
     * Remove idAnnounceUserAnnounceWtb
     *
     * @param \AppBundle\Entity\Announce $idAnnounceUserAnnounceWtb
     */
    public function removeIdAnnounceUserAnnounceWtb(\AppBundle\Entity\Announce $idAnnounceUserAnnounceWtb)
    {
        $this->idAnnounceUserAnnounceWtb->removeElement($idAnnounceUserAnnounceWtb);
    }

    /**
     * Get idAnnounceUserAnnounceWtb
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdAnnounceUserAnnounceWtb()
    {
        return $this->idAnnounceUserAnnounceWtb;
    }
}
