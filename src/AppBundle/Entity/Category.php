<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity (repositoryClass="AppBundle\Entity\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_category", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="name_category", type="string", length=50, nullable=false)
     */
    private $nameCategory;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="idCategoryProduct")
     */
    private $idProductCategory;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idProductCategory = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idCategory
     *
     * @return integer
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }

    /**
     * Set nameCategory
     *
     * @param string $nameCategory
     *
     * @return Category
     */
    public function setNameCategory($nameCategory)
    {
        $this->nameCategory = $nameCategory;

        return $this;
    }

    /**
     * Get nameCategory
     *
     * @return string
     */
    public function getNameCategory()
    {
        return $this->nameCategory;
    }

    /**
     * Add idProductCategory
     *
     * @param \AppBundle\Entity\Product $idProductCategory
     *
     * @return Category
     */
    public function addIdProductCategory(\AppBundle\Entity\Product $idProductCategory)
    {
        $this->idProductCategory[] = $idProductCategory;

        return $this;
    }

    /**
     * Remove idProductCategory
     *
     * @param \AppBundle\Entity\Product $idProductCategory
     */
    public function removeIdProductCategory(\AppBundle\Entity\Product $idProductCategory)
    {
        $this->idProductCategory->removeElement($idProductCategory);
    }

    /**
     * Get idProductCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdProductCategory()
    {
        return $this->idProductCategory;
    }
}
