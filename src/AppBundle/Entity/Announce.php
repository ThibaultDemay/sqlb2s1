<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Announce
 *
 * @ORM\Table(name="announce", indexes={@ORM\Index(name="announce_user_FK", columns={"id_user_announce"}), @ORM\Index(name="announce_product0_FK", columns={"id_product_announce"})})
 * @ORM\Entity (repositoryClass="AppBundle\Entity\AnnounceRepository")
 */
class Announce
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_announce", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAnnounce;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_online_announce", type="datetime", nullable=false)
     */
    private $dateOnlineAnnounce;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_validated_announce", type="datetime", nullable=false)
     */
    private $dateValidatedAnnounce;

    /**
     * @var string
     *
     * @ORM\Column(name="picture_product_announce", type="string", length=500, nullable=false)
     */
    private $pictureProductAnnounce;

    /**
     * @var string
     *
     * @ORM\Column(name="status_product_announce", type="string", length=200, nullable=false)
     */
    private $statusProductAnnounce;

    /**
     * @var string
     *
     * @ORM\Column(name="finish_product_announce", type="string", length=200, nullable=false)
     */
    private $finishProductAnnounce;

    /**
     * @var float
     *
     * @ORM\Column(name="money_announce", type="float", precision=10, scale=0, nullable=false)
     */
    private $moneyAnnounce;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product_announce", referencedColumnName="id_product")
     * })
     */
    private $idProductAnnounce;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user_announce", referencedColumnName="id_user")
     * })
     */
    private $idUserAnnounce;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="idAnnounceUserAnnounceComment")
     * @ORM\JoinTable(name="user_announce_comment",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_announce_user_announce_comment", referencedColumnName="id_announce")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_user_user_announce_comment", referencedColumnName="id_user")
     *   }
     * )
     */
    private $idUserUserAnnounceComment;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="idAnnounceUserAnnounceWtb")
     * @ORM\JoinTable(name="user_announce_wtb",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_announce_user_announce_wtb", referencedColumnName="id_announce")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_user_user_announce_wtb", referencedColumnName="id_user")
     *   }
     * )
     */
    private $idUserUserAnnounceWtb;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idUserUserAnnounceComment = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idUserUserAnnounceWtb = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idAnnounce
     *
     * @return integer
     */
    public function getIdAnnounce()
    {
        return $this->idAnnounce;
    }

    /**
     * Set dateOnlineAnnounce
     *
     * @param \DateTime $dateOnlineAnnounce
     *
     * @return Announce
     */
    public function setDateOnlineAnnounce($dateOnlineAnnounce)
    {
        $this->dateOnlineAnnounce = $dateOnlineAnnounce;

        return $this;
    }

    /**
     * Get dateOnlineAnnounce
     *
     * @return \DateTime
     */
    public function getDateOnlineAnnounce()
    {
        return $this->dateOnlineAnnounce;
    }

    /**
     * Set dateValidatedAnnounce
     *
     * @param \DateTime $dateValidatedAnnounce
     *
     * @return Announce
     */
    public function setDateValidatedAnnounce($dateValidatedAnnounce)
    {
        $this->dateValidatedAnnounce = $dateValidatedAnnounce;

        return $this;
    }

    /**
     * Get dateValidatedAnnounce
     *
     * @return \DateTime
     */
    public function getDateValidatedAnnounce()
    {
        return $this->dateValidatedAnnounce;
    }

    /**
     * Set pictureProductAnnounce
     *
     * @param string $pictureProductAnnounce
     *
     * @return Announce
     */
    public function setPictureProductAnnounce($pictureProductAnnounce)
    {
        $this->pictureProductAnnounce = $pictureProductAnnounce;

        return $this;
    }

    /**
     * Get pictureProductAnnounce
     *
     * @return string
     */
    public function getPictureProductAnnounce()
    {
        return $this->pictureProductAnnounce;
    }

    /**
     * Set statusProductAnnounce
     *
     * @param string $statusProductAnnounce
     *
     * @return Announce
     */
    public function setStatusProductAnnounce($statusProductAnnounce)
    {
        $this->statusProductAnnounce = $statusProductAnnounce;

        return $this;
    }

    /**
     * Get statusProductAnnounce
     *
     * @return string
     */
    public function getStatusProductAnnounce()
    {
        return $this->statusProductAnnounce;
    }

    /**
     * Set finishProductAnnounce
     *
     * @param string $finishProductAnnounce
     *
     * @return Announce
     */
    public function setFinishProductAnnounce($finishProductAnnounce)
    {
        $this->finishProductAnnounce = $finishProductAnnounce;

        return $this;
    }

    /**
     * Get finishProductAnnounce
     *
     * @return string
     */
    public function getFinishProductAnnounce()
    {
        return $this->finishProductAnnounce;
    }

    /**
     * Set moneyAnnounce
     *
     * @param float $moneyAnnounce
     *
     * @return Announce
     */
    public function setMoneyAnnounce($moneyAnnounce)
    {
        $this->moneyAnnounce = $moneyAnnounce;

        return $this;
    }

    /**
     * Get moneyAnnounce
     *
     * @return float
     */
    public function getMoneyAnnounce()
    {
        return $this->moneyAnnounce;
    }

    /**
     * Set idProductAnnounce
     *
     * @param \AppBundle\Entity\Product $idProductAnnounce
     *
     * @return Announce
     */
    public function setIdProductAnnounce(\AppBundle\Entity\Product $idProductAnnounce = null)
    {
        $this->idProductAnnounce = $idProductAnnounce;

        return $this;
    }

    /**
     * Get idProductAnnounce
     *
     * @return \AppBundle\Entity\Product
     */
    public function getIdProductAnnounce()
    {
        return $this->idProductAnnounce;
    }

    /**
     * Set idUserAnnounce
     *
     * @param \AppBundle\Entity\User $idUserAnnounce
     *
     * @return Announce
     */
    public function setIdUserAnnounce(\AppBundle\Entity\User $idUserAnnounce = null)
    {
        $this->idUserAnnounce = $idUserAnnounce;

        return $this;
    }

    /**
     * Get idUserAnnounce
     *
     * @return \AppBundle\Entity\User
     */
    public function getIdUserAnnounce()
    {
        return $this->idUserAnnounce;
    }

    /**
     * Add idUserUserAnnounceComment
     *
     * @param \AppBundle\Entity\User $idUserUserAnnounceComment
     *
     * @return Announce
     */
    public function addIdUserUserAnnounceComment(\AppBundle\Entity\User $idUserUserAnnounceComment)
    {
        $this->idUserUserAnnounceComment[] = $idUserUserAnnounceComment;

        return $this;
    }

    /**
     * Remove idUserUserAnnounceComment
     *
     * @param \AppBundle\Entity\User $idUserUserAnnounceComment
     */
    public function removeIdUserUserAnnounceComment(\AppBundle\Entity\User $idUserUserAnnounceComment)
    {
        $this->idUserUserAnnounceComment->removeElement($idUserUserAnnounceComment);
    }

    /**
     * Get idUserUserAnnounceComment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdUserUserAnnounceComment()
    {
        return $this->idUserUserAnnounceComment;
    }

    /**
     * Add idUserUserAnnounceWtb
     *
     * @param \AppBundle\Entity\User $idUserUserAnnounceWtb
     *
     * @return Announce
     */
    public function addIdUserUserAnnounceWtb(\AppBundle\Entity\User $idUserUserAnnounceWtb)
    {
        $this->idUserUserAnnounceWtb[] = $idUserUserAnnounceWtb;

        return $this;
    }

    /**
     * Remove idUserUserAnnounceWtb
     *
     * @param \AppBundle\Entity\User $idUserUserAnnounceWtb
     */
    public function removeIdUserUserAnnounceWtb(\AppBundle\Entity\User $idUserUserAnnounceWtb)
    {
        $this->idUserUserAnnounceWtb->removeElement($idUserUserAnnounceWtb);
    }

    /**
     * Get idUserUserAnnounceWtb
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdUserUserAnnounceWtb()
    {
        return $this->idUserUserAnnounceWtb;
    }
}
