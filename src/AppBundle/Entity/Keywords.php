<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Keywords
 *
 * @ORM\Table(name="keywords")
 * @ORM\Entity (repositoryClass="AppBundle\Entity\KeywordsRepository")
 */
class Keywords
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_keyword", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idKeyword;

    /**
     * @var string
     *
     * @ORM\Column(name="keyword", type="string", length=50, nullable=false)
     */
    private $keyword;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="idKeywordProduct")
     */
    private $idProductKeyword;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idProductKeyword = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idKeyword
     *
     * @return integer
     */
    public function getIdKeyword()
    {
        return $this->idKeyword;
    }

    /**
     * Set keyword
     *
     * @param string $keyword
     *
     * @return Keywords
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Get keyword
     *
     * @return string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * Add idProductKeyword
     *
     * @param \AppBundle\Entity\Product $idProductKeyword
     *
     * @return Keywords
     */
    public function addIdProductKeyword(\AppBundle\Entity\Product $idProductKeyword)
    {
        $this->idProductKeyword[] = $idProductKeyword;

        return $this;
    }

    /**
     * Remove idProductKeyword
     *
     * @param \AppBundle\Entity\Product $idProductKeyword
     */
    public function removeIdProductKeyword(\AppBundle\Entity\Product $idProductKeyword)
    {
        $this->idProductKeyword->removeElement($idProductKeyword);
    }

    /**
     * Get idProductKeyword
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdProductKeyword()
    {
        return $this->idProductKeyword;
    }
}
