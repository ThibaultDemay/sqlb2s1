<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity (repositoryClass="AppBundle\Entity\ProductRepository")
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_product", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="name_product", type="string", length=50, nullable=false)
     */
    private $nameProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer_product", type="string", length=50, nullable=false)
     */
    private $manufacturerProduct;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="idProductCategory")
     * @ORM\JoinTable(name="category_product",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_product_category", referencedColumnName="id_product")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_category_product", referencedColumnName="id_category")
     *   }
     * )
     */
    private $idCategoryProduct;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Keywords", inversedBy="idProductKeyword")
     * @ORM\JoinTable(name="product_keyword",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_product_keyword", referencedColumnName="id_product")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_keyword_product", referencedColumnName="id_keyword")
     *   }
     * )
     */
    private $idKeywordProduct;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idCategoryProduct = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idKeywordProduct = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idProduct
     *
     * @return integer
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set nameProduct
     *
     * @param string $nameProduct
     *
     * @return Product
     */
    public function setNameProduct($nameProduct)
    {
        $this->nameProduct = $nameProduct;

        return $this;
    }

    /**
     * Get nameProduct
     *
     * @return string
     */
    public function getNameProduct()
    {
        return $this->nameProduct;
    }

    /**
     * Set manufacturerProduct
     *
     * @param string $manufacturerProduct
     *
     * @return Product
     */
    public function setManufacturerProduct($manufacturerProduct)
    {
        $this->manufacturerProduct = $manufacturerProduct;

        return $this;
    }

    /**
     * Get manufacturerProduct
     *
     * @return string
     */
    public function getManufacturerProduct()
    {
        return $this->manufacturerProduct;
    }

    /**
     * Add idCategoryProduct
     *
     * @param \AppBundle\Entity\Category $idCategoryProduct
     *
     * @return Product
     */
    public function addIdCategoryProduct(\AppBundle\Entity\Category $idCategoryProduct)
    {
        $this->idCategoryProduct[] = $idCategoryProduct;

        return $this;
    }

    /**
     * Remove idCategoryProduct
     *
     * @param \AppBundle\Entity\Category $idCategoryProduct
     */
    public function removeIdCategoryProduct(\AppBundle\Entity\Category $idCategoryProduct)
    {
        $this->idCategoryProduct->removeElement($idCategoryProduct);
    }

    /**
     * Get idCategoryProduct
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdCategoryProduct()
    {
        return $this->idCategoryProduct;
    }

    /**
     * Add idKeywordProduct
     *
     * @param \AppBundle\Entity\Keywords $idKeywordProduct
     *
     * @return Product
     */
    public function addIdKeywordProduct(\AppBundle\Entity\Keywords $idKeywordProduct)
    {
        $this->idKeywordProduct[] = $idKeywordProduct;

        return $this;
    }

    /**
     * Remove idKeywordProduct
     *
     * @param \AppBundle\Entity\Keywords $idKeywordProduct
     */
    public function removeIdKeywordProduct(\AppBundle\Entity\Keywords $idKeywordProduct)
    {
        $this->idKeywordProduct->removeElement($idKeywordProduct);
    }

    /**
     * Get idKeywordProduct
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdKeywordProduct()
    {
        return $this->idKeywordProduct;
    }
}
