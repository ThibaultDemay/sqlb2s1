<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnnounceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dateOnlineAnnounce')->add('dateValidatedAnnounce')->add('pictureProductAnnounce')->add('statusProductAnnounce')->add('finishProductAnnounce')->add('moneyAnnounce')->add('idProductAnnounce')->add('idUserAnnounce')->add('idUserUserAnnounceComment')->add('idUserUserAnnounceWtb');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Announce'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_announce';
    }


}
